cwlVersion: v1.0
class: CommandLineTool

# This is only a partial/workflow-specific implementation for the 'flagstat' command

hints:
#- $import: envvar-global.yml
- $import: samtools-docker.yml
- class: InlineJavascriptRequirement

inputs:

  input:
    type: File
    inputBinding:
      position: 4

  output_name:
    type: string

stdout: $(inputs.output_name)

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)


baseCommand: [samtools, flagstat]
