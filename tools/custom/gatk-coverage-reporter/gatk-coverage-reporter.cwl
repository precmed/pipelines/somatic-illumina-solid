cwlVersion: v1.0
class: CommandLineTool

hints:
  #- $import: envvar-global.yml
  - $import: gatk-coverage-reporter-docker.yml
  - class: InlineJavascriptRequirement

inputs:

  vcf:
    type: File
    inputBinding:
      position: 1
      prefix: --vcf

  bam:
    type: File
    inputBinding:
      position: 2
      prefix: --bam

  org:
    type: string
    inputBinding:
      position: 3
      prefix: --org

  dpcalc:
    type: string?
    inputBinding:
      position: 4
      prefix: --dpcalc

  fraglen:
    type: int?
    inputBinding:
      position: 5
      prefix: --fraglen

  fragfac:
    type: int?
    inputBinding:
      position: 6
      prefix: --fragfac

  output_filename:
    type: string?
    inputBinding:
      position: 7
      prefix: --output

  indexoutput:
    type: boolean?
    inputBinding:
      position: 8
      prefix: --indexoutput

  corefraction:
    type: float?
    inputBinding:
      position: 9
      prefix: --corefraction


outputs:
  output:
      type: File
      outputBinding:
          glob: $(inputs.output_filename.concat(".bgz"))

# cromwell overwrites docker's entrypoint
baseCommand: ["Rscript", "/usr/local/src/scripts/runCoverageReporter.R"]
