# Somatic Pipeline

- A CWL workflow implementing the following pipeline:

`bwa mem -M -t 16  /path/to/reference/genome/fa  input_R1.fastq.gz input_R2_001.fastq.gz > output.sam`

`samtools view -@ 16 -Sb output.sam > output.bam`

`samtools fixmate -@ 16 -O bam output.bam output.fixed.bam`

`samtools sort -@ 16 -o output.fixed.sorted.bam output.fixed.bam`

`samtools view -@ 16 -h -F 0x904 -b output.fixed.sorted.bam > output.fixed.sorted.uniq.bam`

`java -jar picard.jar AddOrReplaceReadGroups I=output.fixed.sorted.uniq.bam O=output.fixed.sorted.uniq.rg.bam RGID=1 RGLB=lib1 RGPL=illumina RGPU=unit1 RGSM=output`

`samtools index output.fixed.sorted.uniq.rg.bam`

`samtools flagstat -@ 16 output.fixed.sorted.uniq.rg.bam > output.align.stats.txt`

`bedtools coverage -a trusight-myeloid-amplicon-track.bed -b output.fixed.sorted.uniq.rg.bam -mean > output.coverage.mean.txt`

`samtools view -L trusight-myeloid-amplicon-track.bed -c output.fixed.sorted.uniq.rg.bam > output.count.ontarget.txt`

`samtools view -c output.fixed.sorted.uniq.rg.bam > output.count.total.txt`

`gatk Mutect2 --reference /path/to/reference/genome/fa --input output.fixed.sorted.uniq.rg.bam --tumor-sample output --output output_GATK_variants.vcf.gz > output.Mutect2_4_1_0_0.out 2>&1`

